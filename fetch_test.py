
import github_fetch

result, status_code = github_fetch.get_repository_metadata({ "user" : "monsterovich", "repository" : "qTox"})
if result and status_code == 200:
    print(f"get_repository_metadata works. Status code: {status_code}.")
else:
    print(f"get_repository_metadata failed. Status code: {status_code}.")

result, status_code = github_fetch.get_repository_issues({ "user" : "qTox", "repository" : "qTox", "page" : 0, "state" : "all"})
if result and status_code == 200:
    print(f"get_repository_issues works. Status code: {status_code}.")
else:
    print(f"get_repository_issues failed. Status code: {status_code}.")

result, status_code = github_fetch.get_repository_pull_requests({ "user" : "qTox", "repository" : "qTox", "page" : 0, "state" : "all"})
if result and status_code == 200:
    print(f"get_repository_pull_requests works. Status code: {status_code}.")
else:
    print(f"get_repository_pull_requests failed. Status code: {status_code}.")

result, status_code = github_fetch.get_repository_forks({ "user" : "qTox", "repository" : "qTox", "page" : 0})
if result and status_code == 200:
    print(f"get_repository_forks works. Status code: {status_code}.")
else:
    print(f"get_repository_forks failed. Status code: {status_code}.")


