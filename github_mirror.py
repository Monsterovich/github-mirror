
import sys
import flask
import logging

import config
import github_fetch

app = flask.Flask(__name__)
app.config["DEBUG"] = True

def fetch(get_function, required_fields, optional_fields = []):
    data = {}

    for field in required_fields:
        if field in flask.request.args:
            data[field] = flask.request.args[field]
        else:
            return { "error" : f"No {field} field provided." }, 400
    for field in optional_fields:
        if field in flask.request.args:
            data[field] = flask.request.args[field]
        else:
            data[field] = None

    return get_function(data)

@app.route('/', methods=['GET'])
def root():
    return flask.render_template("api_index.html")

@app.route(f'{config.API_URI}/user_repo', methods=['GET'])
def api_repository():
    result, status_code = fetch(github_fetch.get_repository_metadata, ["user", "repository"])

    if status_code == 404:
        flask.abort(status_code)

    return flask.jsonify(result), status_code

@app.route(f'{config.API_URI}/repo_issues', methods=['GET'])
def api_repository_issues():
    result, status_code = fetch(github_fetch.get_repository_issues, ["user", "repository"], ["page", "state"])
    
    if status_code == 404:
        flask.abort(status_code)

    return flask.jsonify(result), status_code

@app.route(f'{config.API_URI}/repo_pulls', methods=['GET'])
def api_repository_pull_requests():
    result, status_code = fetch(github_fetch.get_repository_pull_requests, ["user", "repository"], ["page", "state"])
    
    if status_code == 404:
        flask.abort(status_code)

    return flask.jsonify(result), status_code

@app.route(f'{config.API_URI}/repo_forks', methods=['GET'])
def api_repository_forks():
    result, status_code = fetch(github_fetch.get_repository_forks, ["user", "repository"], ["page"])

    if status_code == 404:
        flask.abort(status_code)

    return flask.jsonify(result), status_code

if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, datefmt='[%Y-%m-%d %H:%M:%S]', format='%(asctime)s %(levelname)s %(message)s')
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    app.run()
