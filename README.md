## Usage ##

``python3 server.py``

## API Reference ##

- Get repository information: ``/api/v1/resources/user_repo?user=qTox&repository=qTox``
- Get pull requests: ``/api/v1/resources/repo_pulls?user=qTox&repository=qTox?page=1&state=all``
- Get issues: ``/api/v1/resources/repo_issues?user=qTox&repository=qTox?page=1&state=all``
- Get forks: ``/api/v1/resources/repo_forks?user=qTox&repository=qTox?page=1``

