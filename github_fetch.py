
import requests
import requests.exceptions
import logging

def _quote_arg(arg):
    if arg is None:
        return None

    return requests.utils.quote(str(arg))

def _get_repository_data(args, collection_type = None):
    # primary variables
    user = _quote_arg(args["user"])
    repository = _quote_arg(args["repository"])

    # additional arguments
    if collection_type is not None:
        page = _quote_arg(args["page"])

        if collection_type == "issues" or collection_type == "pulls":
            state = _quote_arg(args["state"])

    try:
        url = f"https://api.github.com/repos/{user}/{repository}"
        params = {}

        if collection_type is not None:
            url += f"/{collection_type}"

            # additional arguments
            if page is not None:
                params["page"] = page

            if collection_type == "issues" or collection_type == "pulls":
                if state is not None:
                    params["state"] = state

        logging.info(f"Fetching: {url} {params}")

        response = requests.get(url, params)
        response.raise_for_status()
    except requests.HTTPError as http_err:
        return { "error" : f'HTTP error: {http_err}' }, response.status_code
    except Exception as err:
        return { "error" : f'Other error: {err}' }, 502
    else:
        return response.json(), response.status_code

def get_repository_metadata(args):
    return _get_repository_data(args)

def get_repository_issues(args):
    return _get_repository_data(args, "issues")

def get_repository_pull_requests(args):
    return _get_repository_data(args, "pulls")

def get_repository_forks(args):
    return _get_repository_data(args, "forks")